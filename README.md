# Context Switching

[![pipline_status]][gitlab]
[![coverage_svg]][gitlab]
[![version]][gitlab]

![Example picture][picture]

A browser extension the allow switching between browser tabs. So far only chrome is supported for not maybe more to come later

## Features
* Save opened tabs to files
* Load urls to open in tabs from local files
* Copy all or individual tabs to clipboard
* Copy individual urls to clipboard

### Package
Create deployable zips
package:chrome: creates a prod built zip targeting chrome
package:firefox: creates a prod built zip targeting firefox

## Tasks

### Check style
* check-style: runs all check style checks

### Run tasks
* build: builds the extensions
* test: run all tests
* prod-build: runs build in release mode along with the above and check style


### Dev tasks
* generate-doc: generates documents
* watch: triggers rebuild on source code changes 

[gitlab]: https://gitlab.com/mrmomo/ContextSwitching/-/commits/master
[coverage]: https://gitlab.com/mrmomo/ContextSwitching/-/commits/master
[picture]: resource/image/screenShot/example-picture.png
[version]: https://img.shields.io/badge/version-0.3.1-blue
[coverage_svg]: https://gitlab.com/mrmomo/ContextSwitch/badges/master/coverage.svg
[pipline_status]: https://gitlab.com/mrmomo/ContextSwitch/badges/master/pipeline.svg
