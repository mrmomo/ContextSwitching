# TODO

More of a road map of what things would be nice to have.

## Code quality
1. Adding vue tests for for testing the ui elements 

## Functional 
1. [ ] Stash open tabs into session and restore tem
2. [x] Add search/filter to tab overview
3. [x] Add on focus on click for tab
4. [x] Add on close button on click for tab
5. [ ] Add vue testing for unit/integration tests

note: 3 and 4 might be put into into a context menu

## Visual
* [x] Add a side navigation bar
    * [x] move options here
    * [ ] add session here
* [-] Add highlight on tab hover
* [-] Change the bottom navigation bar in the tab overview
* [ ] Maybe move to an alternative css framework
* [-] Add dark mode