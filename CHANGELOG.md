# Changelog

## 0.3.4
- Moving file download to  the background worker
- opening a window for file upload for firefox
- move tests to vitest for speedier builds
- move parcel for its zero config approach and speedier builds

## 0.3.3
- adding firefox specific manifest and updating README.md

## 0.3.2
- adding a background working in hopes that chrome doesnt complely kill it

## 0.3.1
- Removing options page as there are no currently configurable options
- removing unused code
- moving code around
- moving towards web-extension polyfill instead of a runtime backend
- adding ui tests

## 0.3.0
- making script more cross platform friendly
- Adding option to close/focus tabs
- Adding notification on export
- Adding sidebar
- Moving file upload to sidebar
- remove options as there are no current relevant ones
- major library updates including vue2 -> vue3
- Adding filter view to url
- updating manifest to v3

## 0.2.6
- Options setting
  - allow the configuration on when to use the file upload dialog

## 0.2.5
### Added
- Generating documentation from source code using typedoc
### Changed
- Moving imports to await import to help with code splitting and reusing
- Breaking core into smaller files
- Refactoring mocks in tests

### Fixed
-  Skipping urls where that have empty urls

## 0.2.4
### Changed
- Changing project name to Context Switching

### Added
- Firefox specific api, although firefox supports chrome apis it is preferable to not 100% rely on that
- File dialog when in Firefox and/or on linux due to focus being lost when the file file picker pops up

### Fixed
- Duplicate urls can now be processed


## 0.2.3
### Removed 
- The removal of image.js in the package script
### Fixed
- Fixing not being able to download file export in chrome,due to `undefined`
- Fixing not being able to copy all urls in chrome, it was silently failing

### Changes
- Excluding chrome specific items in the urls being captured

## 0.2.2
### Changes
- Updating manifest with a slightly better description

### Removed
- Unneeded active tag

## 0.2.1
### Changes
- Adding chrome as the runtime target when packing for it.

## 0.2.0
Going from plain html/js to using Vue.js as a frontend and Metro Ui for styling and
a handful of components
### Added
- Adding MIT as the license
- Rewriting the ui elements in Vue.js in order to break ui elements into separate components to make future changes easier
- Adding styling as well as fonts and icons from Metro Ui  
- Raising unit test coverage to 100% for backend elements, The frontend elements will be (hopefully) covered at a later point
- Individual urls can be copied to clipboard
- Eslint will be used to enforce a more coherent coding style
    - adding to webpack , tests  and code where needed in the scripts
- Both a compile and runtime time target for browser
- Better time formatting for the download file with the help of Moment.js
- Packaging of assets into a zip
    - do far just chrome is supported but adding more later should be easier
- Adding a new icon, mainly to have something aside from the default one
- Output is no minified for all assets except manifest

### Removed
- Unused chrome permissions (download and storage)
- Moving toward a generic download approach that should work on most browsers

## 0.1.0
Adding bare minimum features. Mea>nt to be simple plain javascript and html

### Added
- List open tabs
- Save open tabs to file
- Load urls from file into browser
- Copy tabs into clipboard
