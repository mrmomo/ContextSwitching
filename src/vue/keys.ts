import { WindowRepo } from '@ContextSwitch/browser/Windowing'
import { SystemRepo, StorageRepo, TabRepo } from '@ContextSwitch/core/Repository'
import { Emitter } from 'mitt'
import { InjectionKey } from 'vue'

export const System: InjectionKey<SystemRepo> = Symbol('SystemRepo')
export const Storage: InjectionKey<StorageRepo> = Symbol('StorageRepo')
export const Window: InjectionKey<WindowRepo> = Symbol('StorageRepo')
export const Tab: InjectionKey<TabRepo> = Symbol('TabRepo')
export const Event: InjectionKey<Emitter<any>> = Symbol('Event')
