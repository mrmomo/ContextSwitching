import { createRouter, createWebHashHistory } from 'vue-router'
import UrlManagement from '@ContextSwitchUi/ui/UrlManagement.vue'
import FileUpload from '@ContextSwitchUi/ui/FileUpload.vue'

export const routes = [
  { path: '/', component: UrlManagement },
  { path: '/fileUpload', component: FileUpload }
]
export function router() {
  return createRouter({
    history: createWebHashHistory(),
    routes,
  })
}
