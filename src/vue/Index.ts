import { createApp } from 'vue'
import { Event as MitEvent, System, Window, Storage, Tab } from '@ContextSwitchUi/keys'
import Index from '@ContextSwitchUi/ui/Index.vue'
import mitt from 'mitt'
import { BrowserSystemRepo, BrowserTabRepo, BrowserWindowManagement, MemoryStorage } from '@ContextSwitch/browser/Browser'
import { router } from '@ContextSwitchUi/routes'

async function setup() {
  const app = createApp(Index)
  app.use(router())
  app.provide(System, new BrowserSystemRepo())
  app.provide(Storage, new MemoryStorage())
  app.provide(Tab, new BrowserTabRepo())
  app.provide(Window, new BrowserWindowManagement())
  app.provide(MitEvent, mitt())
  app.mount('#app')
}
setup()
