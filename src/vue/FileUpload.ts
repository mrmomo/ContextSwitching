import { createApp } from 'vue'
import { System, Window, Storage, Tab } from '@ContextSwitchUi/keys'
import FileUPload from '@ContextSwitchUi/ui/FileUpload.vue'
import { BrowserSystemRepo, BrowserTabRepo, BrowserWindowManagement, MemoryStorage } from '@ContextSwitch/browser/Browser'
import { router } from '@ContextSwitchUi/routes'

async function setup() {
  const app = createApp(FileUPload)
  app.use(router())
  app.provide(System, new BrowserSystemRepo())
  app.provide(Storage, new MemoryStorage())
  app.provide(Tab, new BrowserTabRepo())
  app.provide(Window, new BrowserWindowManagement())
  app.mount('#app')
}
setup()
