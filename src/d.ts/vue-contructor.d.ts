import { ContextSwitchRuntime } from 'ContextSwitch/Core'
declare module 'vue/types/vue' {
  // Global properties can be declared
  // on the `VueConstructor` interface
  interface VueConstructor {
    $runtime: ContextSwitchRuntime
  }
}
declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $runtime: ContextSwitchRuntime
  }
}
