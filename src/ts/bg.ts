import browser from 'webextension-polyfill'
import { DownloadContent, Message, MessageType, } from '@ContextSwitch/core/Core'
import { isFireFox } from './browser/Browser'

function fromContent(content: string): string {
  if (isFireFox()) {
    return URL.createObjectURL(new Blob([content], { type: 'text/plain' }))
  }
  return `data:'text/plain';base64,${btoa(content)}`
}

async function handleDownload(message: Message<DownloadContent>, _: any, respond: any) {
  console.log(`processing message ${JSON.stringify(message)}`)
  if (message.type !== MessageType.FILE_DOWNLOAD) {
    return
  }
  const url = fromContent(message.payload.content)
  console.log(`using file name ${message.payload.fileName}`)
  await browser.downloads.download({ url, filename: message.payload.fileName, })
  respond({})
}

browser.runtime.onMessage.addListener(handleDownload as any)
