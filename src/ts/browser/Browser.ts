import browser from 'webextension-polyfill'
import { Persistable } from '@ContextSwitch/core/Storage'
import { ExportOptions, StorageRepo, SystemRepo, TabRepo } from '@ContextSwitch/core/Repository'
import { isTabNotEmpty, Tab, Message, DownloadContent, MessageType } from '@ContextSwitch/core/Core'
import * as Clipboard from '@ContextSwitch/core/ClipBoard'
import { centered, WindowRepo } from '@ContextSwitch/browser/Windowing'

/**
 * Portion of memory that will be treated as storage
 */
export class MemoryPersistable<T> implements Persistable<T> {
  private item: T
  /**
   * initial value we are storing
   * @param item initial value
   */
  constructor(item: T) {
    this.item = item
  }

  persist(item: T): Promise<void> {
    this.item = item
    return Promise.resolve()
  }

  async load(): Promise<T> {
    return this.item
  }
}

/**
 * Wraps around the browsers local storage
 */
export class LocalStoragePersistable<T> implements Persistable<T> {
  private key: string
  private readonly storage: Storage = window.localStorage
  /**
   * initial value we are storing
   * @param key key value in the local storage
   */
  constructor(key: string) {
    this.key = key
  }

  persist(item: T): Promise<void> {
    this.storage.setItem(this.key, JSON.stringify(item))
    return Promise.resolve()
  }

  async load(): Promise<T> {
    const item = JSON.parse(this.storage.getItem(this.key) ?? '{}')
    return Promise.resolve(item)
  }
}

/**
 * Portion of memory where tab information will be stored
 */
export class MemoryStorage implements StorageRepo {
  private _tabs: Persistable<Tab[]>
  /** @ignore */
  constructor() {
    this._tabs = new MemoryPersistable([])
  }

  tabs(): Persistable<Tab[]> {
    return this._tabs
  }
}

/**
 * Common browser behavior
 */
/** Common browser functionality */
export class BrowserTabRepo implements TabRepo {
  /** Common Query param, we only ever want normal windows */
  private readonly globalQueryParams = { windowType: 'normal' }

  async setTabs(windowId: number, urls: string[]): Promise<Tab[]> {
    if (urls.length === 0) {
      return []
    }
    const tabs = await this.getTabs(windowId)
    const diff = tabs.length - urls.length
    if (diff === 0) {
      this.updateTabs(tabs, urls)
      return tabs
    }
    const updateTabs = diff > 0 ? tabs.slice(0, -diff) : tabs
    const updateUrls = diff < 0 ? urls.slice(0, diff) : urls
    this.updateTabs(updateTabs, updateUrls)
    if (diff > 0) {
      this.closeTabs(tabs.slice(updateTabs.length))
    } else {
      await this.createTabs(urls.slice(updateTabs.length))
    }
    return await this.getTabs(windowId)
  }

  getAllTabs(windowId: number): Promise<Tab[]> {
    return this.getTabs(windowId, (_) => this.isAcceptableTab(_))
  }

  isAcceptableTab(tab: Tab): boolean {
    return !this.isUnAcceptableTab(tab)
  }

  isUnAcceptableTab(tab: Tab): boolean {
    return [
      /^chrome.*:\/\//,
      /^moz-extension:\/\//,
      /^about:debugging/,
      /^about:devtools/
    ].some(_ => _.test(tab.url))
  }

  updateTabs(tabs: Tab[], urls: string[]): void {
    if (tabs.length !== urls.length) {
      throw new Error(`Tabs len ${tabs.length} is not the same as url len ${urls.length}`)
    }
    tabs.map((tab, i) => [tab.id, urls[i]])
      .forEach((arr: any[]) => {
        browser.tabs.update(arr[0] as number, { url: arr[1] })
      })
  }

  getTabs(windowId: number, shouldAccept: (_: Tab) => boolean = () => true): Promise<Tab[]> {
    return browser.tabs.query(<browser.Tabs.QueryQueryInfoType>{ windowId, ...this.globalQueryParams })
      .then(tabs => {
        return tabs.map(_ => <Tab>_)
          .filter(isTabNotEmpty)
          .filter(shouldAccept)
      })
  }

  async closeTabs(tabs: Tab[]): Promise<void> {
    await browser.tabs.remove(tabs.map(_ => _.id))
  }

  async createTabs(urls: string[]): Promise<Tab[]> {
    return Promise.all(
      urls.map(_ => browser.tabs.create({ url: _ }) as Promise<Tab>)
    )
  }

  focus(tab: Tab) {
    browser.tabs.update(tab.id, { active: true })
  }
}

export class BrowserSystemRepo implements SystemRepo {
  notifyClipboardCopy(result: Clipboard.Result): void {
    const err = (result as Clipboard.Failure).failureReason
    if (err === undefined && (result as Clipboard.Success) === Clipboard.Success.Ok) {
      Metro.notify.create('Copied to clipboard', 'Success', {})
    } else {
      Metro.notify.create(err, `Failure ${err}`, {})
    }
  }

  notifyFileDownloaded(): void {
    Metro.notify.create('exported file', 'Success', {})
  }

  loadFrom(file: File): Promise<string[]> {
    return new Promise((resolve) => {
      const reader = new FileReader()
      reader.onload = () => {
        const contents: string = reader.result as string
        resolve(contents.split(contents.includes('\r\n') ? '\r\n' : '\n')
          .filter((_: string) => _.trim().length > 0))
      }
      reader.readAsText(file)
    })
  }

  async requestDownload(items: Tab[], options: ExportOptions): Promise<void> {
    if (options !== ExportOptions.PlainText) {
      throw new Error(`Not implemented ${options}`)
    }
    const date = new Date().toISOString()
    await browser.runtime.sendMessage(<Message<DownloadContent>>{
      type: MessageType.FILE_DOWNLOAD,
      payload: {
        fileName: `export-${date.substring(0, date.length - 5)}.txt`.replaceAll(':', '_'),
        content: items.map(_ => _.url).join('\n')
      }
    })
  }

  notifyOptionSave(): void {
    Metro.notify.create('Options saved', 'Success', {})
  }

  async copyToClipBoard(text: string): Promise<Clipboard.Result> {
    return navigator.clipboard.writeText(text).then(
      _ => Clipboard.Success.Ok,
      _ => new Clipboard.Failure('Rejected due to no permissions')
    )
  }
}

export class BrowserWindowManagement implements WindowRepo {
  currentWindowId(): Promise<number> {
    return browser.windows.getCurrent().then(_ => _.id!)
  }

  createCenteredPopup(height: number, width: number, url: string): void {
    const center = centered(height, width, url)
    const buffer = 20
    center.width += buffer
    center.height += buffer + 15
    browser.windows.create(<any>center)
  }
}

export function isFireFox(): boolean {
  return browser.runtime.getURL('').startsWith('moz-extension://')
}
