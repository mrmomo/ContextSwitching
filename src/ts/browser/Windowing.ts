export interface Window {
  /** url to open with */
  url: string
  /** type of window, should always be popup */
  type: string
  /** content width */
  width: number
  /** content height */
  height: number
  /** offset from the left of the screen */
  left: number
  /** offset from the top of the screen */
  top: number
}
/**
 * Queries window information and creation
 */
export interface WindowRepo {
  /**
   * Request the current window identifier
   * @returns The current window id
   */
  currentWindowId(): Promise<number>
  /**
   * Request the create a popup window in the center of the screen
   * @param height window height
   * @param width window width
   * @param url url to open in the popup
   */
  createCenteredPopup(height: number, width: number, url: string): void
}
/**
 * Utility function to create a popup window in the center of the screen
 * @param height window height
 * @param width window width
 * @param url url to open
 */
export function centered(height: number, width: number, url: string): Window {
  const left = Math.trunc((screen.width / 2) - (width / 2))
  const top = Math.trunc((screen.height / 2) - (height / 2))
  return {
    url,
    type: 'popup',
    width,
    height,
    left,
    top
  }
}
