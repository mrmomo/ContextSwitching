/** Successful copy of clipboard contents */
export enum Success {
  Ok
}
/**
 * Failure to copy clipboard contents
 */
export class Failure {
  readonly failureReason: string
  /**
   * Why the failure is occurring
   * @param failureReason The reason why it failed
   */
  constructor(failureReason: string) {
    this.failureReason = failureReason
  }
}
/** Alias for the result being a {@link Success} or {@link Failure}  */
export type Result = Success | Failure
