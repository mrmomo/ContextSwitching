/**
 * Abstraction over Needed files to tab manipulation
 */
export interface Tab {
  /** Unique identifier to for a tab in oder to be reference it in operations, cannot be null/undefined */
  id: number
  /** Url that we want to use, can be any protocol */
  url: string
}

/** Checks to see if a variable is false */
export function isTabNotEmpty(tab: Tab): boolean {
  return !isTabEmpty(tab)
}

/** Checks to see if a variable is false */
export function isTabEmpty(tab: Tab): boolean {
  const url = tab.url
  if (url === undefined) {
    return true
  }
  return tab.url.length <= 0
}

export enum MessageType {
  FILE_DOWNLOAD,
  READ_FILE,
}

export interface DownloadContent {
  fileName: string,
  content: string
}

export interface Message<T> {
  type: MessageType,
  payload: T
}
