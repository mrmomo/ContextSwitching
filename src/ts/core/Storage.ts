/**
 * A Window and its dimensions
 */
/** Defines things that can have persistance semantics to them */
export interface Persistable<T> {
  /** Saves item to the under lying storage */
  persist(item: T): Promise<void>
  /**
   *  Reads item from the under lying storage
   * @returns a promise with the read value
   */
  load(): Promise<T>
}
