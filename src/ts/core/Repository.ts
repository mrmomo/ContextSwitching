import { Persistable } from '@ContextSwitch/core/Storage'
import { Result } from '@ContextSwitch/core/ClipBoard'
import { Tab } from '@ContextSwitch/core/Core'

/**
 * Holds things we want to store to keep track of
 */
export interface StorageRepo {
  /**
   * The tabs urls we have queried from the browser we are in
   * @returns a list of the urls that have been saved
   */
  tabs(): Persistable<Tab[]>
}

export enum ExportOptions {
  PlainText
}

/**
 * For requesting the underlying browser for tab information
 */
export interface TabRepo {
  /**
   * Set urls from a list into tabs in the requested window
   * @param windowId id for which window to set it to
   * @param urls urls to set
   */
  setTabs(windowId: number, urls: string[]): Promise<Tab[]>
  /**
   * Get all tabs for a requested window
   * @param windowId window to get the tab information for
   * @returns Tab on the windows
   */
  getAllTabs(windowId: number): Promise<Tab[]>
  /**
   * Wether the tab is acceptable and excluded
   * should be the opposite of {@link isUnAcceptableTab}
   * @param tab tab to test
   */
  isAcceptableTab(tab: Tab): boolean
  /**
   * Wether the tab is un-acceptable and excluded
   * should be the opposite of {@link isAcceptableTab}
   * @param tab tab to test
   */
  isUnAcceptableTab(tab: Tab): boolean
  /**
   * Set urls to these tabs, must be the same length
   * @param tabs tabs to set urls
   * @param urls  urls to set
   */
  updateTabs(tabs: Tab[], urls: string[]): void
  /**
   * Get tabs in a window
   * @param windowId window to get tabs form
   * @param shouldAccept criteria to use to select a tab
   */
  getTabs(windowId: number, shouldAccept: (tab: Tab) => boolean): Promise<Tab[]>
  /**
   * Request tabs to be closed
   * @param tabs tabs to close
   */
  closeTabs(tabs: Tab[]): Promise<void>
  /**
   * Create tabs opened at urls
   * @param urls urls to open
   */
  createTabs(urls: string[]): Promise<Tab[]>
  /**
   * focus tab and brings it to the forefront
   * @param tab tab to focus on
   */
  focus(tab: Tab): void
}

/**
 * Common operation handle by the browser
 */
export interface SystemRepo {
  /**
   * Attempt to copy text to the systems clipboard
   * @param text text to be copied
   * @returns Result with either the success or the failure
   */
  copyToClipBoard(text: string): Promise<Result>;
  /**
   * Prepares file to export items line by line
   * @param items to save
   * @returns exported name
   */
  requestDownload(items: Tab[], options: ExportOptions): Promise<void>;
  /**
   * Loads all lines from a given file
   * @param file Browser file object
   */
  loadFrom(file: File): Promise<string[]>;
  /**
   * Triggers a notification on the clipboard result
   * @param result Result from a clipboard operation
   */
  notifyClipboardCopy(result: Result): void;
  /**
   * Triggers a notification that options have been saved
   */
  notifyOptionSave(): void;
  /**
   * Triggers a notification that a file has been exported
   */
  notifyFileDownloaded(): void
}
