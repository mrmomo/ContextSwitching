import { beforeEach } from 'vitest'

global.chrome = { runtime: { id: 'test' } }

beforeEach(() => {
  vi.clearAllMocks()
})

vi.stubGlobal('browser', {
  tabs: {
    update: vi.fn(),
    query: vi.fn(),
    remove: vi.fn(),
    create: vi.fn(),
  },
  runtime: {
    sendMessage: vi.fn(),
    getURL: vi.fn(() => ({
      startsWith: vi.fn()
    }))
  },
  windows: {
    getCurrent: vi.fn(),
    create: vi.fn()
  }
})

vi.stubGlobal('Metro', {
  notify: {
    create: vi.fn()
  }
})

vi.stubGlobal('navigator', {
  clipboard: {
    writeText: vi.fn()
  }
})

Object.defineProperty(window, 'matchMedia', {
  writable: true,
  value: vi.fn().mockImplementation(query => ({
    matches: false,
    media: query,
    location: vi.fn(() => ({ _get: vi.fn() })),
    onchange: null,
    addListener: vi.fn(), // deprecated
    removeListener: vi.fn(), // deprecated
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    dispatchEvent: vi.fn()
  }))
})
