import { isTabEmpty, isTabNotEmpty } from '@ContextSwitch/core/Core'

describe('Check that tabs can be empty', () => {
  test('If a tab url is empty or undefined it is considered empty', () => {
    const undef = { id: 12, url: undefined }
    expect(isTabEmpty(undef)).toBe(true)
    expect(isTabNotEmpty(undef)).toBe(false)
    const empty = { id: 12, url: '' }
    expect(isTabEmpty(empty)).toBe(true)
    expect(isTabNotEmpty(empty)).toBe(false)
  })
  test('If a tab url is not empty it is not considered empty', () => {
    const nonEmpty = { id: 12, url: '1323' }
    expect(isTabEmpty(nonEmpty)).toBe(false)
    expect(isTabNotEmpty(nonEmpty)).toBe(true)
  })
})
