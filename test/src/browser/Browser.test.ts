import '@Test/global'
import path from 'path'
import fs from 'fs'
import { BrowserSystemRepo, BrowserTabRepo, BrowserWindowManagement, LocalStoragePersistable, MemoryStorage } from '@ContextSwitch/browser/Browser'
import { Failure, Success } from '@ContextSwitch/core/ClipBoard'
import { DownloadContent, Message, MessageType, Tab } from '@ContextSwitch/core/Core'
import browser from 'webextension-polyfill'
import { ExportOptions } from '@ContextSwitch/core/Repository'

function testResources(file: string): string {
  return path.resolve(__dirname, `../../resource/browser/${file}`)
}

describe('Memory storage', () => {
  it('storages has no items when first created', async () => {
    expect(await new MemoryStorage().tabs().load()).toEqual([])
  })

  it('has the items we stored after persisting it', async () => {
    const memory = new MemoryStorage()
    const items: Tab[] = [
      { id: 1, url: 'delta' },
      { id: 2, url: 'charlie' },
      { id: 3, url: 'bravo' }
    ]
    await memory.tabs().persist(items)
    const savedItems = await memory.tabs().load()
    expect(savedItems).toBe(items)
  })
})

describe('Browser extension', () => {
  describe('has been initialized', () => {
    const browserSystemRepo = new BrowserSystemRepo()
    describe('Can notify user the result of certain operations ', () => {
      describe('Updating user of clipboard operations', () => {
        test('If it successful it notifies as a success', () => {
          browserSystemRepo.notifyClipboardCopy(Success.Ok)
          expect(Metro.notify.create).toHaveBeenCalledWith('Copied to clipboard', 'Success', {})
        })
        test('If it failed it notifies as a failure', () => {
          const text = 'because i said so'
          browserSystemRepo.notifyClipboardCopy(new Failure(text))
          expect(Metro.notify.create).toHaveBeenCalledWith(text, `Failure ${text}`, {})
        })
      })
      describe('Updating user of option saving', () => {
        test('If a save was successful to notify', () => {
          browserSystemRepo.notifyOptionSave()
          expect(Metro.notify.create).toHaveBeenCalledWith('Options saved', 'Success', {})
        })
      })
      test('File has been saved', () => {
        browserSystemRepo.notifyFileDownloaded()
        expect(Metro.notify.create).toHaveBeenCalledWith('exported file', 'Success', {})
      })
    })
    describe('Can load url from files', () => {
      test('loads an empty file', async () => {
        const fileContent = fs.readFileSync(testResources('empty.txt'))
        const file: any = new Blob([fileContent], { type: 'text/plain' })
        file.name = 'hello'
        const readSpy = vi.spyOn(FileReader.prototype, 'readAsText')
        const urls = await browserSystemRepo.loadFrom(file)
        expect(readSpy).toBeCalled()
        expect(urls).toEqual([])
      })
      test('loads a file with a single url', async () => {
        const fileContent = fs.readFileSync(testResources('singe-url.txt'))
        const file: any = new Blob([fileContent], { type: 'text/plain' })
        file.name = 'hello'
        const readSpy = vi.spyOn(FileReader.prototype, 'readAsText')
        const urls = await browserSystemRepo.loadFrom(file)
        expect(readSpy).toBeCalled()
        expect(urls).toEqual(['single :('])
      })
      test('loads a file with a multiple url', async () => {
        const fileContent = fs.readFileSync(testResources('multiple-url.txt'))
        const file: any = new Blob([fileContent], { type: 'text/plain' })
        file.name = 'hello'
        const readSpy = vi.spyOn(FileReader.prototype, 'readAsText')
        const urls = await browserSystemRepo.loadFrom(file)
        expect(readSpy).toBeCalled()
        expect(urls).toEqual(['united', 'we stand'])
      })
    })
    describe('Can interact with local storage', () => {
      // Object.defineProperty(window, 'localStorage', { getItem: vi.fn() });
      const testTab: Tab = { id: 123, url: '234' }
      const stringyTestTab = JSON.stringify(testTab)
      const key = 'kay'
      const storage = new LocalStoragePersistable<Tab>(key)
      test('saves data from local storage', async () => {
        // eslint-disable-next-line no-proto
        const setItem = vi.spyOn(<any>window.localStorage.__proto__, 'setItem')
        await storage.persist(testTab)
        expect(setItem).toBeCalledWith(key, stringyTestTab)
      })
      test('reads data from local storage', async () => {
        // eslint-disable-next-line no-proto
        const getItem = vi.spyOn(<any>window.localStorage.__proto__, 'getItem')
        vi.mocked(window.localStorage.getItem).mockReturnValue(stringyTestTab)
        const value = await storage.load()
        expect(getItem).toBeCalledWith(key)
        expect(value).toEqual(testTab)
      })
    })
    describe('Can prepare files for download only as plain text', () => {
      const output = ExportOptions.PlainText
      test('if there are no items to be saved the file should be empty', async () => {
        await browserSystemRepo.requestDownload([], output)
        const message = <Message<DownloadContent>>vi.mocked(browser.runtime.sendMessage).mock.calls[0][0]
        expect(message.type).toEqual(MessageType.FILE_DOWNLOAD)
        expect(message.payload.content).toEqual('')
      })
      test('Saving a single item should just have that item', async () => {
        const text = 'single-itemized-feather'
        browserSystemRepo.requestDownload([<any>{ url: text }], output)
        const message = <Message<DownloadContent>>vi.mocked(browser.runtime.sendMessage).mock.calls[0][0]
        expect(message.type).toEqual(MessageType.FILE_DOWNLOAD)
        expect(message.payload.content).toEqual(text)
      })
      test('Saving a multiple items as plain text saves all', async () => {
        const items = [
          'f1',
          'f2',
          'f3',
        ]
        browserSystemRepo.requestDownload(items.map(_ => <any>{ url: _ }), output)
        const message = <Message<DownloadContent>>vi.mocked(browser.runtime.sendMessage).mock.calls[0][0]
        expect(message.type).toEqual(MessageType.FILE_DOWNLOAD)
        expect(message.payload.content).toEqual(items.join('\n'))
      })
      test('Non plain text fails', async () => {
        await expect(async () => await browserSystemRepo.requestDownload([], <any>-1))
          .rejects.toEqual(new Error('Not implemented -1'))
      })
    })
    describe('Can copy text to the browser\'s clipboard', () => {
      const copyText = 'heck yeah'
      test('if the copy was a success it results is a success', async () => {
        vi.mocked(navigator.clipboard.writeText).mockResolvedValue(undefined)
        const result = await browserSystemRepo.copyToClipBoard(copyText)
        expect(result).toBe(Success.Ok)
      })
      test('if the copy was a failure it results is a failure', async () => {
        vi.mocked(navigator.clipboard.writeText).mockRejectedValue(new Error())
        const result = await browserSystemRepo.copyToClipBoard(copyText)
        expect(result).toEqual(new Failure('Rejected due to no permissions'))
      })
    })
  })
  describe('When dealing with tabs', () => {
    const browserTab = new BrowserTabRepo()
    const DEFAULT_TABS: Tab[] = [
      { id: 1, url: 'hehehehehe' },
      { id: 2, url: 'told ya so' },
      { id: 12, url: 'chrome://newtab' },
      { id: 19, url: 'chrome-extension://something-else' }
    ]
    describe('It can query tabs', () => {
      it('Returns a list of all of the open tabs', async () => {
        vi.mocked(browser.tabs.query).mockResolvedValue(<any>DEFAULT_TABS)
        const windowId = 908
        const allTabs = await browserTab.getAllTabs(windowId)
        expect(allTabs).toEqual(allTabs)
        expect(browser.tabs.query).toBeCalledWith({ windowId, windowType: 'normal' })
      })
      it('It ignores tabs with no urls', async () => {
        const windowId = 9082345
        vi.mocked(browser.tabs.query).mockResolvedValue(<any>[{ url: '' }])
        const tabs = await browserTab.getAllTabs(windowId)
        expect(tabs).toEqual([])
        expect(browser.tabs.query).toBeCalledWith({ windowId, windowType: 'normal' })
      })
    })
    describe('It manipulates tabs', () => {
      test('It can close tabs', async () => {
        await browserTab.closeTabs(DEFAULT_TABS)
        expect(browser.tabs.remove).toBeCalledWith(DEFAULT_TABS.map(_ => _.id))
      })
      test('It can new create tabs', async () => {
        const first = 'first'
        const second = 'second'
        vi.mocked(browser.tabs.create).mockResolvedValue(undefined)
        const tabs = await browserTab.createTabs([first, second])
        expect(tabs).toHaveLength(2)
        expect(browser.tabs.create).toBeCalledWith({ url: first })
        expect(browser.tabs.create).toBeCalledWith({ url: second })
      })
      test('It can focus a single tab', () => {
        const tab: Tab = { id: 34, url: 'fictional' }
        browserTab.focus(tab)
        expect(browser.tabs.update).toBeCalledWith(tab.id, { active: true })
      })
      describe('It import tabs from a list', () => {
        test('If there are no tabs nothing happens', async () => {
          vi.mocked(browser.tabs.query).mockResolvedValue(<any>DEFAULT_TABS)
          const windowId = 14080
          await browserTab.setTabs(windowId, [])
          expect(browser.tabs.remove).not.toBeCalled()
          expect(browser.tabs.create).not.toBeCalled()
        })
        test('attemps to update non equal tabs throws error', () => {
          expect(() => browserTab.updateTabs(DEFAULT_TABS, []))
            .toThrow('Tabs len 4 is not the same as url len 0')
        })
        test('If there are equal items and tabs, tabs are only updated', async () => {
          vi.mocked(browser.tabs.query).mockResolvedValue(<any>DEFAULT_TABS)
          const windowId = 1408
          const newOne = 'new one'
          const newTwo = 'new two'
          const delta = 'delta'
          const phi = 'phi'
          await browserTab.setTabs(windowId, [newOne, newTwo, delta, phi])
          expect(browser.tabs.update).toBeCalledWith(1, { url: newOne })
          expect(browser.tabs.update).toBeCalledWith(2, { url: newTwo })
          expect(browser.tabs.update).toBeCalledWith(12, { url: delta })
          expect(browser.tabs.update).toBeCalledWith(19, { url: phi })
          expect(browser.tabs.remove).not.toBeCalled()
          expect(browser.tabs.create).not.toBeCalled()
          expect(browser.tabs.query).toBeCalledWith({ windowId, windowType: 'normal' })
        })
        test('If there are more items than tabs it grows to accommodate', async () => {
          vi.mocked(browser.tabs.query).mockResolvedValue(<any>DEFAULT_TABS)
          const windowId = 14
          const one = '1st'
          const two = '2nd'
          const three = '3rd'
          const fish = 'fishy'
          const theNewOne = 'shiny'
          await browserTab.setTabs(windowId, [one, two, three, fish, theNewOne])
          expect(browser.tabs.remove).not.toBeCalled()
          expect(browser.tabs.update).toBeCalledWith(1, { url: one })
          expect(browser.tabs.update).toBeCalledWith(2, { url: two })
          expect(browser.tabs.update).toBeCalledWith(12, { url: three })
          expect(browser.tabs.update).toBeCalledWith(19, { url: fish })
          expect(browser.tabs.create).toBeCalledWith({ url: theNewOne })
          expect(browser.tabs.query).toBeCalledWith({ windowId, windowType: 'normal' })
        })
        test('If there are more tabs then items it shrinks to that many items', async () => {
          vi.mocked(browser.tabs.query).mockResolvedValue(<any>DEFAULT_TABS)
          const windowId = 354567
          const text = 'there can only be one'
          await browserTab.setTabs(windowId, [text])
          expect(browser.tabs.update).toBeCalledWith(1, { url: text })
          expect(browser.tabs.remove).toBeCalledWith([2, 12, 19])
          expect(browser.tabs.create).not.toBeCalled()
          expect(browser.tabs.query).toBeCalledWith({ windowId, windowType: 'normal' })
        })
      })
      describe('It process duplicate urls', () => {
        const url = 'http://www.not-same.com'
        const repeatingUrl = Array.from(Array(20).keys()).map(_ => url)
        test('The duplicate is already in an open tab', async () => {
          vi.mocked(browser.tabs.query).mockResolvedValue(<any>[{ id: 223563, url }])
          await browserTab.setTabs(24, repeatingUrl)
          await expect(browser.tabs.create).toBeCalledTimes(19)
          await expect(browser.tabs.remove).not.toBeCalled()
        })
        test('A duplicate entry should be remove if already present', async () => {
          const tabs = Array.from(Array(20).keys()).map(_ => <Tab>{ id: _, url: repeatingUrl[_] })
          vi.mocked(browser.tabs.query).mockResolvedValue(<any>tabs)
          await browserTab.setTabs(234, [url])
          await expect(browser.tabs.remove).toBeCalledWith(tabs.map(_ => _.id).slice(1))
          await expect(browser.tabs.create).not.toBeCalled()
        })
        test('The duplicate is not in an open tab', async () => {
          vi.mocked(browser.tabs.query).mockResolvedValue([])
          await browserTab.setTabs(24, repeatingUrl)
          await expect(browser.tabs.create).toBeCalledTimes(20)
          await expect(browser.tabs.remove).not.toBeCalled()
        })
      })
      describe('Accepts some kind of tabs', () => {
        test.each([
          'chrome://newtab',
          'chrome-extension://something-else',
          'moz-extension://someone-eleses-extension',
          'about:debugging://osome',
          'about:devtools://dev'
        ])('%s should be skipped', url => {
          const newTab = <Tab>{ url }
          expect(browserTab.isAcceptableTab(newTab)).toBe(false)
          expect(browserTab.isUnAcceptableTab(newTab)).toBe(true)
        })
      })
    })
  })
  describe('It can handle windows', () => {
    const windowManagement = new BrowserWindowManagement()
    describe('It can ask for current window id ', () => {
      it('Returns a list of all of the open tabs', async () => {
        const expectedWindowId = 43
        vi.mocked(browser.windows.getCurrent)
          .mockResolvedValue(<any>{ id: expectedWindowId })
        const number = await windowManagement.currentWindowId()
        expect(number).toEqual(expectedWindowId)
        expect(browser.windows.getCurrent).toHaveBeenCalledWith()
      })
    })
    describe('It can create a popup', () => {
      it('Returns a list of all of the open tabs from another window', async () => {
        const height = 500
        const width = 200
        const expectedTop = 710
        const expectedLeft = 440
        Object.defineProperty(global.screen, 'width', {
          get: function () {
            return 1080
          }
        })
        Object.defineProperty(global.screen, 'height', {
          get: function () {
            return 1920
          }
        })
        const url = 'http://yelew.com'
        windowManagement.createCenteredPopup(height, width, url)
        expect(browser.windows.create).toBeCalledWith({
          left: expectedLeft,
          type: 'popup',
          top: expectedTop,
          height: height + 35,
          width: width + 20,
          url
        })
      })
    })
  })
})
