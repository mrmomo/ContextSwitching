import '@Test/global'
import { mount, VueWrapper } from '@vue/test-utils'
import { BrowserSystemRepo, BrowserTabRepo, BrowserWindowManagement, MemoryStorage } from '@ContextSwitch/browser/Browser'
import mitt from 'mitt'
import browser from 'webextension-polyfill'
import { Event, System, Storage, Tab as TabKey, Window } from '@ContextSwitchUi/keys'
import { Tab } from '@ContextSwitch/core/Core'
import { Router } from 'vue-router'

export function fullMount(component: any, windowId: number, tabs: Tab[], routerPlugin: Router = null): [VueWrapper, any] {
  vi.mocked(browser.windows.getCurrent).mockResolvedValue(<any>{ id: windowId })
  vi.mocked(browser.tabs.query).mockResolvedValue(<any>tabs)
  const ends: Record<any, any> = {
    [System as symbol]: new BrowserSystemRepo(),
    [Storage as symbol]: new MemoryStorage(),
    [TabKey as symbol]: new BrowserTabRepo(),
    [Window as symbol]: new BrowserWindowManagement(),
    [Event as symbol]: mitt(),
  }
  return [mount(component, {
    global: {
      plugins: routerPlugin ? [routerPlugin] : null,
      provide: {
        ...ends
      }
    },
  }), ends]
}
