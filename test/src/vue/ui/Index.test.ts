import { fullMount } from '@Test/src/vue/app'
import Index from '@ContextSwitchUi/ui/Index.vue'
import { router } from '@ContextSwitchUi/routes'

describe('File upload is loaded', () => {
  describe('menu has items', () => {
    const wrapper = fullMount(Index, 1, [], router())[0]
    const menuItems = wrapper.get('.navview-menu').findAll('li')
    test.each([
      { pos: 1, routes: '#/', iconPath: 'image/icon/home.svg', caption: 'Home' },
      { pos: 2, routes: '#/fileUpload', iconPath: 'image/icon/upload.svg', caption: 'Import File' },
    ])('$caption points to $routes with icon $iconPath', ({ pos, routes, iconPath, caption }) => {
      const item = menuItems[pos]
      expect(item.get('a').attributes('href')).toEqual(routes)
      expect(item.get('img').attributes('src')).toEqual(iconPath)
      expect(item.get('.caption').text()).toEqual(caption)
    })
  })
  describe('main page is loaded', () => {
    const wrapper = fullMount(Index, 1, [], router())[0]
    test('url management is loaded', () => {
      expect(wrapper.find('#url-management').exists()).toBeTruthy()
      expect(wrapper.find('#file-management').exists()).toBeFalsy()
    })
  })
  describe('side page is loaded', () => {
    const vueRouter = router()
    const wrapper = fullMount(Index, 1, [], vueRouter)[0]
    test('can switch to file upload', async () => {
      await vueRouter.push('/fileUpload')
      expect(wrapper.find('#file-management').exists()).toBeTruthy()
      expect(wrapper.find('#url-management').exists()).toBeFalsy()
    })
  })
})
