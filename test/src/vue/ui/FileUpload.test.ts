import '@Test/global'
import FileUpload from '@ContextSwitchUi/ui/FileUpload.vue'
import browser from 'webextension-polyfill'
import { fullMount } from '@Test/src/vue/app'

describe('file upload page is loaded', () => {
  const wrapper = fullMount(FileUpload, 1, [])[0]
  globalThis.window.location = <any>vi.fn()
  test.each([
    { lineEnding: 'CRLF', content: 'one\r\ntwo' },
    { lineEnding: 'LF', content: 'one\ntwo' },
  ])('line ending $lineEnding is loaded', async (file) => {
    Object.assign(global.window.close, vi.fn())
    const changedFile: any = new File([file.content], 'meh')
    const onChange: any = (<any>wrapper).componentVM.loadFromFile
    await onChange({ target: { files: [changedFile] } })
    expect(browser.tabs.create).toHaveBeenNthCalledWith(1, { url: 'one' })
    expect(browser.tabs.create).toHaveBeenNthCalledWith(2, { url: 'two' })
  })
})
