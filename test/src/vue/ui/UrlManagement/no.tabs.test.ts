import '@Test/global'
import browser from 'webextension-polyfill'
import { DownloadContent, Message, MessageType } from '@ContextSwitch/core/Core'
import { fullMount } from '@Test/src/vue/app'
import UrlManagement from '@ContextSwitchUi/ui/UrlManagement.vue'

describe('Url management of no available tabs', () => {
  const wrapper = fullMount(UrlManagement, 1, [])[0]
  test('the list is empty', () => {
    expect(wrapper.get('#url-list').html())
      .toEqual('<div id="url-list"></div>')
  })
  test('downloads an empty file on export all', async () => {
    const button = wrapper.find('#exportAllToFile')
    await button.trigger('click')
    const message = <Message<DownloadContent>>vi.mocked(browser.runtime.sendMessage).mock.calls[0][0]
    expect(message.type).toEqual(MessageType.FILE_DOWNLOAD)
    expect(message.payload.content).toEqual('')
  })
  test('copies an empty string to clipboard', async () => {
    vi.mocked(navigator.clipboard.writeText).mockResolvedValue(undefined)
    const button = wrapper.find('#copyAllToClipboard')
    await button.trigger('click')
    expect(navigator.clipboard.writeText)
      .toBeCalledWith('')
  })
})
