import '@Test/global'
import { DownloadContent, Message, MessageType, Tab } from '@ContextSwitch/core/Core'
import { fullMount } from '@Test/src/vue/app'
import UrlManagement from '@ContextSwitchUi/ui/UrlManagement.vue'
import browser from 'webextension-polyfill'

describe('There are some tabs loaded', () => {
  const tabs: Tab[] = [
    { id: 0, url: 'triple' },
    { id: 1, url: 'quadruple' },
  ]
  const wrapper = fullMount(UrlManagement, 1, tabs)[0]
  test('there are tabs loaded', () => {
    expect(wrapper.find('#url-list').findAll('.url-item')).toHaveLength(2)
  })
  describe('bottom panel actions can be triggered', () => {
    const expectedOutput = tabs.map(_ => _.url).join('\n')
    test('export to file matches expected content', async () => {
      const button = wrapper.find('#exportAllToFile')
      await button.trigger('click')
      const message = <Message<DownloadContent>>vi.mocked(browser.runtime.sendMessage).mock.calls[0][0]
      expect(message.type).toEqual(MessageType.FILE_DOWNLOAD)
      expect(message.payload.content).toEqual(expectedOutput)
    })
    describe('exports to clipboard', () => {
      test('export to clipboard matches expected content', async () => {
        vi.mocked(navigator.clipboard.writeText).mockResolvedValue(undefined)
        const button = wrapper.find('#copyAllToClipboard')
        await button.trigger('click')
        expect(navigator.clipboard.writeText)
          .toBeCalledWith(expectedOutput)
      })
    })
  })
})
