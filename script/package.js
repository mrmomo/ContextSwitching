/* eslint-disable @typescript-eslint/no-var-requires */
const AdmZip = require('adm-zip')
const fs = require('fs')
const appRoot = require('app-root-path').path
const path = require('path')
const dist = path.resolve(path.resolve(appRoot, 'dist'))
const zip = new AdmZip()

fs.readdirSync('./dist').forEach(item => {
  const itemPath = path.resolve(dist, item)
  if (fs.lstatSync(itemPath).isFile()) {
    zip.addLocalFile(itemPath)
  } else {
    zip.addLocalFolder(itemPath, item)
  }
})
zip.writeZip('./context-switch.zip')
