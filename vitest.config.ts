import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import path from 'path'

export default defineConfig({
  plugins: [vue({
    template: {
      transformAssetUrls: {
        img: [],
      }
    }
  })],
  test: {
    globals: true,
    environment: 'jsdom',
  },
  resolve: {
    alias: {
      '@ContextSwitch': path.resolve(__dirname, 'src/ts'),
      '@Test': path.resolve(__dirname, 'test'),
      '@ContextSwitchUi': path.resolve(__dirname, 'src/vue'),
      '@ContextResource': path.resolve(__dirname, 'resource'),
    }
  }
})
